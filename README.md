# httpsvol

This is a docker-compose repo that C&CZ uses to make shares available via HTTPS.

You connect a container to a share, and launch this docker-compose with that container.
The caddy config expects the share to be mounted in the environment variable $SHARE,
see the compose.yml file

## AddContainer

This repo can be added with:

~~~ sh
AddContainer installshare miek containervm02 https://gitlab.science.ru.nl/cncz/sys/dockerdepot/httpsvol
~~~

Using `miek` here, but should be a system user in the end.

Then connect a share:

~~~
# lv|grep install
 684 zwik2:install             zfs  vol     ok:ok          100T 2.0T  75.20% rw:cncz ro:cncz                  🗹    🗹   🗹    🗹 ☐      🗹
~~~

And do a `ChContainer installshare --volume 684`.

You need a DNS name for this container and SHARE and NETWORK need to be setup in the environment.
SHARE must be the NFS name of the share. And NETWORK can basically be anything.

`ChContainer --env "SHARE=/data/installshare" installshare` and
`ChContainer --env "NETWORK=myinstallshare" installshare`
